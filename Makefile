build: ##@Docker install services dependencies
	docker-compose -f infrastructure/docker/docker-compose-build.yml up --build
up: ##@Docker Build and deploy services
	docker-compose -f infrastructure/docker/docker-compose-local.yml --project-name cardtowaripayaccount-business-process-service up --build -d
start: ##@Docker Start service
	docker-compose -f infrastructure/docker/docker-compose-local.yml --project-name cardtowaripayaccount-business-process-service start
stop: ##@Docker Stop service
	docker-compose -f infrastructure/docker/docker-compose-local.yml --project-name cardtowaripayaccount-business-process-service stop
connect-php: ##@Docker Connect on the container php-fpm
	docker exec -it cardtowaripayaccount-business-process-service-fpm_1 /bin/bash
