@Library('TSI@container-ic')
import org.tsi.pipeline.Script
import org.tsi.pipeline.PipelineFactory
import org.tsi.pipeline.Pipeline
import org.tsi.pipeline.notification.NotificationSystem
import org.tsi.pipeline.build.Build
import org.tsi.pipeline.Environment

node {

    Script.environment = this

	properties([
		parameters(
			[
				choice(
					choices: Environment.getEnvironmentsList(),
					description: 'Please enter the environment',
					name: 'TARGET_ENVIRONMENT'
				),

				string(
					defaultValue: '',
					description: 'Please enter a tag name',
					name: 'TAG_NAME'
				),

				booleanParam(
					defaultValue: true,
					description: 'Deploy ?',
					name: 'DEPLOY'
				),

				/*booleanParam(
					defaultValue: false,
					description: 'Run tests ?',
					name: 'RUN_TESTS'
				),

				booleanParam(
					defaultValue: false,
					description: 'Are you deploying onto a PCI ZONE?',
					name: 'PCI_ZONE'
				),*/

				string(
					defaultValue: 'cardtowaripayaccount-business-process-service',
					description: 'Project name',
					name: 'PROJECT_NAME'
				),

				string(
					defaultValue: 'git@bitbucket.org:tsipayment/cardtowaripayaccount-business-process-service.git',
					description: 'Repo url',
					name: 'GIT_URL'
				),

				string(
                    defaultValue: 'https://infra-console.tsirec1.fr',
                    description: 'Infra console url',
                    name: 'INFRA_CONSOLE_URL'
                )

			]
		)
	])


	withEnv([
		'ANSIBLE_SCRIPT_PATH=infrastructure/ansible',
		'PIPELINE_TYPE=docker'
	]){

        try{

			Pipeline pipeline = PipelineFactory.ForEnvironment(params.TARGET_ENVIRONMENT);

			stage('Checkout') {
				pipeline.checkout()
			}

			env.releaseVersion = pipeline.getReleaseVersion();
			echo 'The release version is: ' + env.releaseVersion

            stage ('ansible-playbooks') {
				pipeline.prepare()
			}

            stage('prepare') {
            parallel(
			'Composer PHP': {
			    Build.buildWithPlayBook = false
				pipeline.build()
			},
            'Generate secrets': {
            	pipeline.generateConfigFiles()
            },
            )
            }

			stage('Create artefact') {
				if(params.DEPLOY) {
					pipeline.createArtefact()
				}
			}

            stage('Publish artefact') {
                if(params.DEPLOY) {
                    pipeline.publish()
                }
            }

			stage('Deploy') {
				if(params.DEPLOY) {
					pipeline.deploy()
				}
			}

			stage('Clean up') {
                pipeline.clean()
            }

		}catch(error){
           println(error.getMessage())
		}
	}
}
